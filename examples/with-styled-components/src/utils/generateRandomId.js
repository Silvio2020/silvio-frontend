
const generateRandomId = (min, max) => (
    Math.random() * (max - min) + min
)

export default generateRandomId;