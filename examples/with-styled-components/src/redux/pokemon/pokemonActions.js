import {
    CAPTURE_POKEMON,
    LET_POKEMON_GO,
    FETCH_POKEMON_REQUESTS,
    FETCH_POKEMON_SUCCESS,
    FETCH_POKEMON_ERROR,
} from './pokemonType';

export const capturePokemon = (pokemon) => {
    return {
        type: CAPTURE_POKEMON,
        payload: pokemon
    }
}

export const letPokemonGo = (pokemon) => {
    return {
        type: LET_POKEMON_GO,
        payload: pokemon
    }
}

export const fetchPokemonRequests = () => {
    return {
        type: FETCH_POKEMON_REQUESTS
    }
}

export const fetchPokemonSuccess = (pokemons) => {
    return {
        type: FETCH_POKEMON_SUCCESS,
        payload: pokemons
    }
}

export const fetcPokemonsError = (error) => {
    return {
        type: FETCH_POKEMON_ERROR,
        payload: error
    }
}