import styled from 'styled-components';

export const SpanGradient = styled.span`
    position: absolute;
    left: 0px;
    right: 0px;
    top: 0px;
    bottom: 303px;                       
    background: linear-gradient(90deg, #43E97B 0%, #38F9D7 100%);
`;

export const BorderEffect = styled.span`
    position: absolute;
    left: 0px;
    right: 0px;
    top: 95px;
    bottom: 295px;
    background: #fff;
    border-top-right-radius: 50px;
    border-top-left-radius: 50px;
`;

export const FormView = styled.form`
    position: absolute;

    div {
        display: flex;
        flex-direction: column;
        align-items: center;
        width: 358px;
        position: relative;
        top: 385px;
    }
    
`;