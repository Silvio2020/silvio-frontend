import React, { useState } from "react";

import Sidebar from "components/Sidebar";
import Modal from '../../components/ModalPoekmon'
import iconFront from '../../assets/images/ashFront.png'

import * as S from "./styled";


const MapPage = () => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  return (
    
    <S.MapWrapper className="map">
      <Sidebar />
        <div>
          <a href="#" onClick={() => setModalIsOpen(true)}>
            <img src={iconFront} alt="" />
          </a>
        </div>
        <Modal isOpen={modalIsOpen} onRequestClose={() => setModalIsOpen(false)} />
    </S.MapWrapper>
  )
}

export default MapPage;
