import axios from 'axios';

import {
   fetchPokemonRequests,
   fetchPokemonSuccess,
   fetchPokemonsError 
} from '../redux/pokemon/pokemonActions';

import generateRandomId from '../utils/generateRandomId'

export const fetchPokemons = () => {
    return async (dispatch) => {
        try {
            dispatch(fetchPokemonRequests());
            const response = await axios.get(`https://pokeapi.co/api/v2/pokemon/${generateRandomId(1, 807)}`);
            dispatch(fetchPokemonSuccess(response))

        } catch(error) {
            const errorMsg = error.message
            dispatch(fetchPokemonsError(errorMsg))
        }

    }
} 