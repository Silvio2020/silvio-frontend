import {
    CAPTURE_POKEMON,
    LET_POKEMON_GO,
    FETCH_POKEMON_REQUESTS,
    FETCH_POKEMON_SUCCESS,
    FETCH_POKEMON_ERROR,
} from './pokemonType';

const initialState = {
    loading: false,
    pokemons: [],
    error: ''
}

const pokemonReducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_POKEMON_REQUESTS:
            return {
                ...state,
                loading: true
            }
        case FETCH_POKEMON_SUCCESS:
            return {
                loading: false,
                pokemons: action.payload,
                error: ''
            }
        case FETCH_POKEMON_ERROR:
            return {
                loading: false,
                pokemons: [],
                error: action.payload
            }
        default:
            return state
    }
}

export default pokemonReducer;