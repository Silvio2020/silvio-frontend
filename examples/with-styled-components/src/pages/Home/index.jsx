import React from "react";
import { Link } from 'react-router-dom'
import Button from '../../components/Button';
import pokemonImg from '../../assets/images/pokemonLogo.png'

import * as S from "./styled";


const HomePage = () => (
  <S.HomeWrapper>
    <div>
      <img src={pokemonImg} alt="logo" />
      <Link to="/map">
        <Button bg="red" text="start" />
      </Link>
    </div>
  </S.HomeWrapper>
);

export default HomePage;
