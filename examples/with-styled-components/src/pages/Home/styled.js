import styled from "styled-components";

export const HomeWrapper = styled.div`
    display: flex;
    align-items: center;
    justify-content: center;

    background: linear-gradient(90deg, #43E97B 0%, #38F9D7 100%);
    width: 100%;
    height: 50vw;

    div {
        display: flex;
        flex-direction: column;
        align-items: center;

        margin-top: 250px;
        margin-bottom: 250px;

        img {
            margin-bottom: 20px
        }
    }


`;
