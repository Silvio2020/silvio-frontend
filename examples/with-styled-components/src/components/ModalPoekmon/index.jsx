import React from 'react';
import Modal from 'react-modal';
import Button from '../Button';

import { ButtonSmall } from '../Button/styled';

import { BorderEffect, SpanGradient, FormView, } from './styled';


Modal.setAppElement('#root')
const ModalPokemon = ({ isOpen, onRequestClose,  }) => {
    return (
        <>
            <Modal 
                isOpen={isOpen} 
                onRequestClose={onRequestClose}

                style={{
                    overlay: {
                        background: 'rgba(0, 0, 0, 0.35)',
                        backgroundBlendMode: 'multiply',
                    },
                    content: {
                        color: 'orange',
                        width: 360,
                        height: 559,
                        margin: '0 auto',
                        borderRadius: 8,
                        padding: 0,
                    }
                    
                }}
            >   
                <>
                   <SpanGradient></SpanGradient>
                   <BorderEffect></BorderEffect>
                    <FormView>
                        <div>
                            <Button bg="red" text="Liberar pokemon" />
                        </div>
                        <div style={{ flexDirection: 'row', justifyContent: 'center' }}>
                           <ButtonSmall> 
                               <Button bg="green" text="Planta" /> 
                            </ButtonSmall>
                            <ButtonSmall> 
                               <Button bg="blue" text="Veneno" /> 
                            </ButtonSmall>
                        </div>
                    </FormView>
                </>
            </Modal>
        </>
    )
}

export default ModalPokemon
