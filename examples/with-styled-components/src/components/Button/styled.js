import styled from "styled-components";

export const ButtonWrapper = styled.button`
  padding: 1.6rem 2.4rem;
  background-color: ${props => props.bg === 'red' ? '#ff3d71' : props.bg === 'green' ? '#67AF32' : '#924990' };
  border: 1px solid ${props => props.bg === 'red' ? '#ff3d71' : props.bg === 'green' ? '#67AF32' : '#924990'};
  border-radius: 42px;
  font-weight: bold;
  font-size: 1.6rem;
  line-height: 1.6;
  text-align: center;
  color: #ffffff;
  cursor: pointer;
  transition: all 0.2s;
  text-transform: uppercase;

  &.icon {
    display: flex;
    align-items: center;
    justify-content: center;
    width: 64px;
    height: 64px;
    border: 4px solid ${props => props.bg === 'red' ? '#ff3d71' : props.bg === 'green' ? '#67AF32' : '#924990'};
  }

  &:hover {
    background-color: ${props => props.bg === 'red' ? '#db2c66' : props.bg === 'green' ? '#67AB78' : '#965285'};
  }
`;

export const Icon = styled.img``;

export const Text = styled.span`
  font-family: 'Open Sans', sans-serif;
`;


export const ButtonSmall = styled.span`
    padding: 10px;
    
    button {
      padding: 15px;
      font-size: 14px;
    }
`
